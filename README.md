
![Lipchain](https://cdn-images-1.medium.com/max/1600/1*kenEgXl62HjDPN6JOXLsxw.jpeg)


LIPCHAIN WILL BECOME THE NEXUS OF THE GLOBAL SURFING COMMUNITY OF 60 M LN PEOPLE
BY PROVIDING A DECENTRALIZED SOCIAL NETWORK AND YEAR - AROUND RESORTS FOR TRAINING.

##  Features!

  - No fees for transactions made with LIPS tokens inside our blockchain
  - Competitive rates for transactions with other tokens and crypto currencies


## Our Blockchain
We are developing our own blockchain with Alfa-version release in 11/2018 and
revolutionary features such as:
- First to use biometric face & voice recognition engines
- Multi-currency wallet with biometric security with the possibility of exchange.
- Faster transactions: +100k transactions per second (Bitcoin@7tps, Ethereum@14tps)
- Platform includes a proprietary wallet/exchange (no fees for LIPS exchange)
- User can control their video and can block public access to them
